
public class Zelle 
{
   private Baum baum;
   private double naehrstoffgehalt;
   private double wassergehalt;
   private boolean hatParasiten = false;
   int wasserRadius;
   
   
   public Zelle()
   {
	   
   }
   
   public double getNaehrstoffgehalt()
   {
	   return naehrstoffgehalt;
   }
   public boolean hatBaum()
   {
	   return baum != null;
   }
   public Baum getBaum()
   {
	   return baum;
   }
   public boolean hatParasiten()
   {
	   return hatParasiten;
   }
   public double getWassergehalt()
   {
	   return wassergehalt;
   }
   
   public int calculateWasserRadius() {
	   double zähler = baum.getHealth()*baum.getHoehe()*baum.getDurst();
	   double nenner = Math.pow(baum.getAlter(), 2);
	   int rate = (int) Math.round(zähler/nenner);
	   if (rate > 2000)
	   {
		   return 3;
	   }
	   else if(rate > 1000)
	   {
		   return 2;
	   }
	   else if(rate > 500){
		   return 1;
	   }
	   else
	   {
		   return 0;
	   }
   }
}
