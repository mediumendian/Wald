import javax.swing.JFrame;

public abstract class Baum 
{
   /**
	 *  
	 */
 private double alter;
 private double hoehe;
 private double health; // in Prozent
 private double durst;
 private Zelle zugehoerigeZelle;
 int lebensdauer;
 
 public Baum(Zelle z) 
 {
	 this.alter = 0;
	 this.hoehe = 0;
	 this.health = 100;
	 this.zugehoerigeZelle = z;
 }
 public Zelle getZugehoerigeZelle()
 {
	 return this.zugehoerigeZelle;
 }
 public void altern()
 {
	 
 }
public void faellen()
{
	
}
public double getAlter() {
	return alter;
}

public void erhoeheAlter(double alter) 
{
	this.alter += alter;
}

public double getHoehe() 
{
	return hoehe;
}

private void setHoehe(double hoehe) 
{
	this.hoehe = hoehe;
}

public double getHealth() 
{
	return health;
}

public void setHealth(double health) 
{
	this.health = health;
}

public void setDurst(double durst)
{
	this.durst = durst;
}

public double getDurst()
{
	return this.durst;
}
}
