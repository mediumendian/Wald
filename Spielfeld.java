
public abstract class Spielfeld 
{
  private static Zelle [][] waldboden;
  private static boolean istErzeugt = false;
  private static int Zellengröße;
  
  public static void erzeugeSpielfeld(int laengeX, int laengeY, int Zellengröße)
  // Brauchen wir hier die Zellengröße schon?
  {

	  waldboden = new Zelle[laengeX][laengeY];
	  istErzeugt = true;
	  Spielfeld.Zellengröße = Zellengröße;
  }
  
  /**
   *Noch nicht benutzt
   */
  public static int getZellengröße() 
  {
	  
	  return Zellengröße;
  }
  
  public static boolean istErzeugt()
  {
	  return istErzeugt;
  }
  public static void baumFaellen(int x, int y)
  {
	  if(Spielfeld.hatZelleBaum(x, y))
	  {
		  Spielfeld.getZelle(x, y).getBaum().faellen();
	  }
  }
  public static boolean hatZelleBaum(int x, int y)
  {
	 try
	 {
		 return waldboden[x][y].hatBaum();		 
	 }
	 catch(ArrayIndexOutOfBoundsException e){
		 
	 }
	 return false;
  }

  public static int getBreite()
  {
	  if(Spielfeld.istErzeugt())
	  return waldboden.length;
	  else
	  return 0;
  }
  public void aktualisieren()
  {
	  for(int x = 0; x < waldboden.length; x++)
		  for(int y = 0; y < waldboden[0].length;y++)
		  {
			  if(Spielfeld.getZelle(x, y).hatBaum())
				  Spielfeld.getZelle(x, y).getBaum().altern();
		  }
  }
  public static int getHoehe()
  {
	  if(Spielfeld.istErzeugt())
	  return waldboden[0].length;
	  else
	  return 0;
  }
  public static Zelle getZelle(int x, int y)
  {
	  if(Spielfeld.istErzeugt())
	  return waldboden[x][y];
	  else
	  return null;
  }
}
